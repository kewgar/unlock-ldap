/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import static unlockldapaccount.Controller.logger;
import static unlockldapaccount.Model_Data.LDAP_URL;
import static unlockldapaccount.Model_Data.adminName;
import static unlockldapaccount.Model_Data.adminPass;
import static unlockldapaccount.Model_Data.ctx;
import static unlockldapaccount.Model_Data.env;
import static unlockldapaccount.Model_Data.trushstorePass;
import static unlockldapaccount.Model_Data.trushstorePath;

/**
 *
 * @author 006284
 */
public class TestSSLConnect {

    public static void main(String[] args) {
        System.setProperty("javax.net.ssl.trustStore", "C:\\Program Files\\Java\\jdk1.8.0_141\\jre\\lib\\security\\cacerts");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

        // Setting the LDAP connection information
        env = new Hashtable<String, String>();
        env.put(Context.PROVIDER_URL, "ldaps://localhost:24636");
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "cn=root");
        env.put(Context.SECURITY_CREDENTIALS, "P@ssw0rd");
        ctx = null;

        try {
            // Openning the connection
            ctx = new InitialDirContext(env);
            System.err.println("Can connected LDAP");
            logger.debug("Can connected LDAP");
            ctx.close();

            // Use your context here...
        } catch (NamingException e) {
            System.out.println("Problem occurs during context initialization !");
            e.printStackTrace();
            logger.error(e);
        }
    }
}
