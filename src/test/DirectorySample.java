/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author 006284
 */
import java.util.Enumeration;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class DirectorySample {
	public DirectorySample() {

	}

	public void doLookup() {
		Properties properties = new Properties();
		properties.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		properties.put(Context.PROVIDER_URL, "ldap://localhost:10101");
		properties.put(Context.SECURITY_AUTHENTICATION,"simple");
		properties.put(Context.SECURITY_PRINCIPAL,"uid=imadmin,ou=people,ou=im,ou=ca,o=com"); 
		properties.put(Context.SECURITY_CREDENTIALS,"ytiruces@ITID01");
		try {
			DirContext context = new InitialDirContext(properties);
                        String ret[] = {"uid","imEmployeeStatus"};
                        
			SearchControls searchCtrls = new SearchControls();
                        searchCtrls.setReturningAttributes(ret);
			searchCtrls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String filter = "(cn=*)";
			NamingEnumeration values = context.search("ou=people,ou=im,ou=ca,o=com",filter,searchCtrls);
                        int i = 0;
			while (values.hasMoreElements())
			{
				SearchResult result = (SearchResult) values.next();
				Attributes attribs = result.getAttributes();

				if (null != attribs)
				{
					for (NamingEnumeration ae = attribs.getAll(); ae.hasMoreElements();)
					{
						Attribute atr = (Attribute) ae.next();
						String attributeID = atr.getID();
						for (Enumeration vals = atr.getAll(); 
							vals.hasMoreElements(); 
							System.out.println(attributeID +": "+ vals.nextElement()));
					}
                                        
				}
                                i++;
			}

			context.close();

		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		DirectorySample sample = new DirectorySample();
		sample.doLookup();
	}

}
