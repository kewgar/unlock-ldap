package unlockldapaccount;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import java.io.FileNotFoundException;
import static unlockldapaccount.Controller.logger;

public class ReadProperties extends Model_Data {

    public static void SetPath() {
        PasswordSecrurity pass = new PasswordSecrurity();
        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = new FileInputStream("/home/config/ce/unlockldapaccount/LDAP_Java.properties");
            // load a properties file
            prop.load(input);
            // get the property value and print it out
            trushstorePath = (prop.getProperty("trushstorePath"));
            trushstorePass = (prop.getProperty("trushstorePass"));
            LDAP_URL = (prop.getProperty("LDAP_URL"));
            adminName = (prop.getProperty("adminName"));
            adminPass = pass.decode(prop.getProperty("adminPass"));
            searchAttribute = (prop.getProperty("searchAttribute"));
            baseDN = (prop.getProperty("baseDN"));
            logLocation = (prop.getProperty("logLocation"));

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            logLocation = "/home/config/ce";
            e.printStackTrace();
            logger.error(e);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error(ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    logger.error(e);
                }
            }
        }

    }

}
