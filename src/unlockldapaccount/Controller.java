/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unlockldapaccount;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import org.apache.log4j.*;
import static unlockldapaccount.LDAPS_Authen.*;
import static unlockldapaccount.LDAP_Method.*;
import static unlockldapaccount.Model_Data.*;

/**
 *
 * @author 006284
 */
public class Controller {

    public static Logger logger = Logger.getLogger(Controller.class);

    public static void main(String[] args) {

        // TODO code application logic here
        ReadProperties readProperties = new ReadProperties();
        readProperties.SetPath();
        String ts = new SimpleDateFormat("MM-dd-yyyy").format(Calendar.getInstance().getTime());
        try {
//            String filePath = "D:\\IDG_Input_Files\\SmartServe\\ProcessFile\\log\\SSlog" + ts + ".log";
            String filePath = logLocation + "/LDAP_Method_" + ts + ".log";
            PatternLayout layout = new PatternLayout("%-5p %d %m%n");
            RollingFileAppender appender = new RollingFileAppender(layout, filePath);
            appender.setName("myFirstLog");
            appender.setMaxFileSize("10MB");
            appender.activateOptions();
            Logger.getRootLogger().addAppender(appender);

        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("start new log for " + ts);

        LDAP_Authen();

        try {
            LdapContext context = new InitialLdapContext(env, null);
            String user = args[0].replace(" ", "");
            String function = args[1].replace(" ", "");
            String dn = FineDN(user, env);
            if (function.contains("enable")) {
                if ((args[2].equalsIgnoreCase("true") || args[2].equalsIgnoreCase("false"))) {
                    Enable(dn, env, args[2]);
                } else {
                    System.err.println("2nd parameter accept only 'true' or 'false'");
                    logger.error("2nd parameter accept only 'true' or 'false'");
                }
            } else if (function.contains("unlock")) {
                UnlockAccount(dn, context, env);
            } else if (function.contains("test")) {
                editThFuc(dn, env, args[2]);
            } else {
                logger.error("Unknow Medtod");
                logger.error("Argrument infomation " + args.length + " " + user + " " + function);
            }

            //test
//            String dn = FineDN("K0212224", env);
//            UnlockAccount(dn, context, env);
//            Enable(dn, env, "true");
        } catch (NamingException e) {
            System.out.println("Problem occurs during context initialization !");
            e.printStackTrace();
            logger.error(e);
        }

    }

}
