/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unlockldapaccount;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import static unlockldapaccount.Controller.logger;

/**
 *
 * @author 006284
 */
public class LDAPS_Authen extends Model_Data {

    public static void main(String[] args) throws IOException {

//        String trushstorePath = "C:\\Program Files\\Java\\jdk1.8.0_141\\jre\\lib\\security\\cacerts";
        String trushstorePath = "C:\\Program Files\\Java\\jdk1.8.0_141\\jre\\lib\\security\\ca2";
        System.setProperty("javax.net.ssl.trustStore", trushstorePath);
        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

        // Setting the LDAP connection information
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.PROVIDER_URL, "ldaps://localhost:63657");
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "cn=root");
        env.put(Context.SECURITY_CREDENTIALS, "P@ssw0rd");
        ctx = null;

        try {
            // Openning the connection
            ctx = new InitialDirContext(env);
            System.err.println("Can connected LDAP");
            logger.debug("Can connected LDAP");
            ReadUserFromLDAP(ctx);
            ctx.close();

            // Use your context here...
        } catch (NamingException e) {
            System.out.println("Problem occurs during context initialization !");
            e.printStackTrace();
            logger.error(e);
        }
    }

    public static ArrayList<HashMap<String, String>> ReadUserFromLDAP(DirContext ctx) throws IOException, NamingException {
        HashMap<String, String> mapLDAP = null;

        ArrayList<HashMap<String, String>> userList = new ArrayList<>();
        SearchControls searchCtls = new SearchControls();

        String returnedAtts[] = {"*", "+"};
        searchCtls.setReturningAttributes(returnedAtts);
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
//        String searchFilter = "uid=K0588311";
        String searchFilter = "uid=*";
        String searchBase = "DC=KBANK,DC=COM";

        NamingEnumeration<SearchResult> results = ctx.search(searchBase, searchFilter, searchCtls);
        while (results != null && results.hasMoreElements()) {
            mapLDAP = new HashMap<String, String>();
            SearchResult entry = (SearchResult) results.next();
//            Attributes attributes = ctx.getAttributes(entryDN, new String[] {"*", "+"});
            Attributes attrs = entry.getAttributes();
            System.out.println(attrs.get("ibm-latestBindTimestamp"));

        }
        ctx.close();
        logger.info("[Step LDAP 3.2] Active users : " + userList.size());
        return userList;
    }

    public static void LDAPS_Authen() {
        System.setProperty("javax.net.ssl.trustStore", trushstorePath);
        System.setProperty("javax.net.ssl.trustStorePassword", trushstorePass);

        // Setting the LDAP connection information
        env = new Hashtable<String, String>();
        env.put(Context.PROVIDER_URL, LDAP_URL);
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, adminName);
        env.put(Context.SECURITY_CREDENTIALS, adminPass);
        ctx = null;

        try {
            // Openning the connection
            ctx = new InitialDirContext(env);
            System.err.println("Can connected LDAP");
            logger.debug("Can connected LDAP");
            ctx.close();

            // Use your context here...
        } catch (NamingException e) {
            System.out.println("Problem occurs during context initialization !");
            e.printStackTrace();
            logger.error(e);
        }
    }

    public static void LDAP_Authen() {
        System.setProperty("javax.net.ssl.trustStore", trushstorePath);
        System.setProperty("javax.net.ssl.trustStorePassword", trushstorePass);

        // Setting the LDAP connection information
        env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, LDAP_URL);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, adminName);
        env.put(Context.SECURITY_CREDENTIALS, (adminPass));

        ctx = null;

        try {
            // Openning the connection
            ctx = new InitialDirContext(env);
            System.err.println("Can connected LDAP");
            logger.debug("Can connected LDAP");
            ctx.close();

            // Use your context here...
        } catch (NamingException e) {
            System.out.println("Problem occurs during context initialization !");
            e.printStackTrace();
            logger.error(e);
        }
    }

}
