/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unlockldapaccount;

import java.util.Hashtable;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.naming.ldap.LdapContext;
import static unlockldapaccount.Controller.logger;
import static unlockldapaccount.Model_Data.*;

/**
 *
 * @author 006284
 */
public class LDAP_Method {
    public static void UnlockAccount(String dn, LdapContext context, Hashtable<String, String> env) {

        try {
            ctx = new InitialDirContext(env);
            MyLdapKControl myKControl = new MyLdapKControl();
            context.setRequestControls(myKControl.getControls());

            ModificationItem[] modItems = new ModificationItem[2];
            modItems[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("pwdAccountLockedTime"));
            modItems[1] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("pwdFailureTime"));
            context.modifyAttributes(dn, modItems);
            System.err.println("Unlock user " + dn + " successful.");
            logger.debug("Unlock user " + dn + " successful.");
            ctx.close();
        } catch (NamingException e) {
            System.out.println("Problem occurs during context initialization !");
            System.err.println("Unlock user " + dn + " failed.");
            logger.debug("Unlock user " + dn + " failed.");
            logger.error(e);
            e.printStackTrace();
        }
    }

    public static String FineDN(String uid, Hashtable<String, String> env) {

        try {
            ctx = new InitialDirContext(env);
            // Create the initial context
            SearchControls sc = new SearchControls();
            sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration res = ctx.search(baseDN, searchAttribute+"=" + uid, sc);
            String dn = "";
            
            if(!res.hasMore())
            {
                System.err.println("Can not found user in LDAP for "+searchAttribute+" = "+uid);
                logger.debug("Can not found user in LDAP for "+searchAttribute+" = "+uid);
            }
            while (res.hasMore()) {
                SearchResult s = (SearchResult) res.next();

                // print user's DN
                System.err.println("The user "+searchAttribute +" = "+dn+" DN is " + s.getNameInNamespace());
                logger.debug("The user "+searchAttribute +" = "+dn+" DN is " + s.getNameInNamespace());
                dn = s.getNameInNamespace();
            }

            // Close the context when we're done
            ctx.close();
            return dn;

        } catch (NamingException e) {
            System.out.println("Problem occurs during context initialization !");
            e.printStackTrace();
            logger.error(e);
            return "";
        }
    }

    public static void Enable(String dn, Hashtable<String, String> env, String enable) {

        try {
            // Openning the connection
            ctx = new InitialDirContext(env);
            
            // Use your context here...
            ModificationItem[] mods = new ModificationItem[1];
            Attribute mod0 = new BasicAttribute("ibm-pwdAccountLocked", enable);
            mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, mod0);
            ctx.modifyAttributes(dn, mods);
            System.err.println("Enable = "+enable+ " successful.");
            logger.debug("Enable = "+enable+ " successful.");

            // Close the context when we're done
            ctx.close();
            
        } catch (NamingException e) {
            System.err.println("Enable = "+enable+ " failed.");
            logger.debug("Enable = "+enable+ " failed.");
            System.out.println("Problem occurs during context initialization !");
            e.printStackTrace();
            logger.error(e);
        }

    }
    
    public static void editThFuc(String dn, Hashtable<String, String> env, String enable) {

        try {
            // Openning the connection
            ctx = new InitialDirContext(env);
            
            // Use your context here...
            ModificationItem[] mods = new ModificationItem[1];
            Attribute mod0 = new BasicAttribute("thaifunc", enable);
            mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, mod0);
            ctx.modifyAttributes(dn, mods);
            System.err.println("Enable = "+enable+ " successful.");
            logger.debug("Enable = "+enable+ " successful.");

            // Close the context when we're done
            ctx.close();
            
        } catch (NamingException e) {
            System.err.println("Enable = "+enable+ " failed.");
            logger.debug("Enable = "+enable+ " failed.");
            System.out.println("Problem occurs during context initialization !");
            e.printStackTrace();
            logger.error(e);
        }

    }

}
