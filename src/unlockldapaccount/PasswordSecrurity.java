/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unlockldapaccount;

import java.util.Base64;

/**
 *
 * @author 006284
 */
public class PasswordSecrurity {

    public String decode(String p) {
        byte[] decodedBytes = Base64.getDecoder().decode(p);
        String decodedString = new String(decodedBytes);

        return decodedString;
    }
}
